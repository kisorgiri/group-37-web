import { ErrorHandler } from "../utils/error.handler"
import { httpClient } from "../utils/httpClient"
import { notify } from "../utils/notify"

export const ProductActionTypes = {
    SET_IS_LOADING: 'SET_IS_LOADING',
    PRODUCTS_RECEIVED: 'PRODUCTS_RECEIVED',
    PROUDCT_REMOVED: 'PRODUCT_REMOVED',
    SET_IS_PRODUCT_LOADING: 'SET_IS_PRODUCT_LOADING',
    PRODUCT_RECEIVED: 'PRODUCT_RECEIVED',
    SET_IS_REVIEW_SUBMITTING: 'SET_IS_REVIEW_SUBMITTING'
}

// export const fetchProducts_ac = params=>{
//     return (dispatch)=>{

//     }
// }

// export function fetchProducts_ac(params) {
//     return function (dispatch) {

//     }
// }

export const fetchProducts_ac = (params) => (dispatch) => {
    console.log('at action file >>>', params)
    dispatch(loading(true))
    httpClient.GET('/product', true)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PRODUCTS_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            ErrorHandler(err)
        })
        .finally(() => {
            dispatch(loading(false))
        })
}

export const removeProduct_ac = (id) => async (dispatch) => {
    try {
        await httpClient.DELETE(`/product/${id}`, true)
    } catch (err) {
        return ErrorHandler(err)
    }
    notify.showInfo("Product Removed")
    dispatch({
        type: ProductActionTypes.PROUDCT_REMOVED,
        payload: id
    })
}

export const fetchProduct_ac = (id, showLoader = true) => dispatch => {
    if (showLoader) {
        dispatch({
            type: ProductActionTypes.SET_IS_PRODUCT_LOADING,
            payload: true
        })
    }

    httpClient.GET(`/product/${id}`, true)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PRODUCT_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            ErrorHandler(err)
        })
        .finally(() => {
            if (showLoader) {
                dispatch({
                    type: ProductActionTypes.SET_IS_PRODUCT_LOADING,
                    payload: false
                })
            }
        })
}

export const addReview_ac = (reviewData, productId) => dispatch => {
    dispatch({
        type: ProductActionTypes.SET_IS_REVIEW_SUBMITTING,
        payload: true
    })
    httpClient.POST(`/product/add_review/${productId}`, reviewData, true)
        .then(response => {
            dispatch(fetchProduct_ac(productId, false))
        })
        .catch(err => {
            ErrorHandler(err)
        })
        .finally(() => {
            dispatch({
                type: ProductActionTypes.SET_IS_REVIEW_SUBMITTING,
                payload: false
            })
        })
}

const loading = isLoading => ({
    type: ProductActionTypes.SET_IS_LOADING,
    payload: isLoading
})