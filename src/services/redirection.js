export const redirectToLogin = (history) => {

}

export const redirectToDashboard = (role, history) => {
    let redirectPath = '';
    switch (role) {
        case 1:
            redirectPath = 'admin'
            break;
        case 2:
            redirectPath = 'dashboard'
            break;
        default:
            break;
    }
    history.push(redirectPath)

}

export const redirectToHome = (role, history) => {

}