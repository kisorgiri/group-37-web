import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BASE_URL,
    responseType: 'json',
    timeout: 20000,
    timeoutErrorMessage: 'Request Timeout'
})

const getHeaders = (isSecured = false) => {
    let options = {
        'Content-Type': 'application/json'
    }

    if (isSecured) {
        options['Authorization'] = `Bearer ${localStorage.getItem('token')}`
    }
    return options;
}

const GET = (url, isSecured = false, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecured),
        params
    })
}

const POST = (url, data, isSecured = false, params = {}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecured),
        params
    })
}

const PUT = (url, data, isSecured = false, params = {}) => {
    return http.put(url, data, {
        headers: getHeaders(isSecured),
        params
    })
}

const DELETE = (url, isSecured = false, params = {}) => {
    return http.delete(url, {
        headers: getHeaders(isSecured),
        params
    })
}

const UPLOAD = (method, url, data = {}, files = []) => {
    return new Promise((resolve, reject) => {
        // for uploading files we are using xmlhttprequest
        // we are sending value as formData
        const xhr = new XMLHttpRequest();
        const formData = new FormData();


        // append files in form data
        // this will work for single file and multiple
        files.forEach(item => {
            formData.append('images', item, item.name)
        })

        // append textual data in formdata
        for (let key in data) {
            formData.append(key, data[key])
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response)
                }
            }
        }

        xhr.open(method, `${BASE_URL}${url}?token=Bearer ${localStorage.getItem('token')}`, true)
        xhr.send(formData)
    })

}

export const httpClient = {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}