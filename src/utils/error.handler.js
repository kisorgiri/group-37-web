import { notify } from './notify';
export const ErrorHandler = (error) => {
    // handle all of your application error in this method
    // set an debugger;
    let errMsg = 'Something Went Wrong';
    debugger; // once debugger is found your applicaiton will be paused
    let err = error && error.response;
    errMsg = err && err.data && err.data.msg;
    // TODO check another error object
    if (typeof (errMsg) === 'object') {
        errMsg = extractDbErrorMessage(errMsg)
    }
    // error parser for database  error

    return notify.showError(errMsg)

    // errMsg is object
    // prepare a function to parse database error

    // steps to follow
    // check error
    // parse error
    // extract error message
    // show them in ui
}

function extractDbErrorMessage(error) {
    // error is object with database error
    let err;
    switch (error.code) {
        case 11000:
            let key = Object.keys(error.keyValue)[0]
            let value = error.keyValue[key];
            err = `${value} already taken for property ${key}`
            break;

        default:
            err = 'something wrong with database'
            break;
    }

    return err;
}