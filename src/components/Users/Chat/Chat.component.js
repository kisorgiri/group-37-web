import React, { Component } from 'react';
import * as io from 'socket.io-client';
import { relativeTime } from '../../../utils/dateUtil';
import './Chat.component.css'
import { notify } from './../../../utils/notify'

const SOCKET_URL = process.env.REACT_APP_SOCKET_URL;

const defaultMessageBody = {
    senderId: '',
    senderName: '',
    message: '',
    receiverId: '',
    receiverName: '',
    time: ''
}

export class Chat extends Component {
    constructor() {
        super()

        this.state = {
            msgBody: {
                ...defaultMessageBody
            },
            messages: [],
            users: []
        }
    }
    componentDidMount() {
        this.socket = io(SOCKET_URL)
        this.currentUser = JSON.parse(localStorage.getItem('user'))
        this.runSocket();
    }

    runSocket = () => {
        this.socket.emit('new-user', this.currentUser.username)
        this.socket.on('reply-message-own', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages
            })
        })
        this.socket.on('reply-message', (data) => {
            const { messages, msgBody } = this.state;
            // swap sender as receiver
            msgBody.receiverId = data.senderId
            messages.push(data);
            this.setState({
                messages,
                msgBody
            })
        })
        this.socket.on('users', users => {
            this.setState({
                users: users
            })
        })
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                [name]: value
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        // prepare message body
        // send through socket
        const { msgBody, users } = this.state;
        msgBody.senderName = this.currentUser.username;
        msgBody.time = Date.now()
        if (!msgBody.receiverId) {
            return notify.showInfo('please select a user to continue')
        }
        users.forEach((user, index) => {
            if (user.name === this.currentUser.username) {
                msgBody.senderId = user.id
            }
        })

        this.socket.emit('new-message', msgBody)
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                message: ''
            }
        }))
    }

    selectUser = user => {
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    render() {
        return (
            <div>
                <h2>Let's Chat</h2>
                <div className="row">
                    <div className="col-md-6">
                        <h2>Messages</h2>
                        <section className="msger">
                            <header className="msger-header">
                                <div className="msger-header-title">
                                    <i className="fas fa-comment-alt"></i> selected user/group
                                </div>
                                <div className="msger-header-options">
                                    <span><i className="fas fa-cog"></i></span>
                                </div>
                            </header>

                            <main className="msger-chat">
                                {
                                    this.state.messages.map((message, index) => (
                                        <div key={index} className="msg left-msg">
                                            <div
                                                className="msg-img"
                                            >
                                            </div>

                                            <div className="msg-bubble">
                                                <div className="msg-info">
                                                    <div className="msg-info-name">{message.senderName}</div>
                                                    <div className="msg-info-time">{relativeTime(message.time, 'minute')}</div>
                                                </div>

                                                <div className="msg-text">
                                                    {message.message}
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }

                            </main>
                            <form className="msger-inputarea" onSubmit={this.handleSubmit}>
                                <input type="text" name="message" value={this.state.msgBody.message} className="msger-input" placeholder="Enter your message..." onChange={this.handleChange}></input>
                                <button type="submit" className="msger-send-btn">Send</button>
                            </form>
                        </section>
                    </div>
                    <div className="col-md-6 scroll-area">
                        <h2>Users</h2>
                        {this.state.users.map((user, index) => (
                            <div key={index} className="msg left-msg ">
                                <div
                                    className="msg-img"
                                >
                                </div>
                                <button className="btn btn-default" onClick={() => this.selectUser(user)}>{user.name}</button>

                            </div>
                        ))}
                    </div>

                </div>

            </div>
        );
    }
}

