// functional component

// 17 > import of react is optional
import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import './Header.component.css';

const logout = (history) => {
    // challanges redirect to login
    // props match and location
    // clear localstorage
    localStorage.clear();
    history.push('/')
}
const HeaderComponent = (props) => {
    const currentUser = JSON.parse(localStorage.getItem('user'))
    // console.log('props in header >>>', props)
    // incoming arguments are props
    let content = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item"><NavLink activeClassName="selected" to="/dashboard">Dashboard</NavLink> </li>
            <li className="nav_item"><NavLink activeClassName="selected" to="/about">About</NavLink> </li>
            <li className="nav_item"><NavLink activeClassName="selected" to="/contact">contact</NavLink> </li>
            <li className="nav_item"><NavLink activeClassName="selected" to="/settings/hi">Settings</NavLink> </li>
            <li className="nav_item logout">
                <span style={{ marginRight: '5px' }}>{currentUser.username}</span>
                <button onClick={() => logout(props.history)} className="btn btn-success">Logout</button>
            </li>
        </ul>
        :
        <ul className="nav_list">
            <li className="nav_item"><NavLink activeClassName="selected" to="/dashboard">HOME</NavLink> </li>
            <li className="nav_item"><NavLink activeClassName="selected" exact to="/">LOGIN</NavLink> </li>
            <li className="nav_item"><NavLink activeClassName="selected" to="/register">REGISTER</NavLink> </li>
        </ul>
    return (
        <div className="nav_bar">
            {content}
        </div>
    )
}

export const Header = withRouter(HeaderComponent)
