import React from 'react';
export const PageNotFound = (props) => {
    return (
        <div>
            <h2>Page Not Found</h2>
            <img src="/images/notfound.jpg" alt="notfound.png" width="400px"></img>
        </div>
    )
}

