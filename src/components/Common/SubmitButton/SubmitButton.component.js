import React from 'react';

export const SubmitButton = (props) => {
    const enabledLabel = props.enabledLabel || 'Submit';
    const disabledLabel = props.disabledLabel || 'Submitting...';

    let btn = props.isSubmitting
        ? <button disabled className="btn btn-info" >{disabledLabel}</button>
        : <button disabled={props.isDisabled} type="submit" className="btn btn-primary" >{enabledLabel}</button>

    return btn;
}
