import React from 'react';
import { NavLink } from 'react-router-dom';
import './Sidebar.component.css';

export const Sidebar = (props) => {
    return (
        <div className="sidebar">
            <NavLink  to="/dashboard">Dashboard</NavLink>
            <NavLink  to="/add_product">Add Product</NavLink>
            <NavLink  to="/view_products">View Product</NavLink>
            <NavLink  to="/search_product">Search Product</NavLink>
            <hr></hr>
            <NavLink  to="/chat">Messages</NavLink>
            <NavLink  to="/notifications">Notifications</NavLink>
        </div>
    )
}