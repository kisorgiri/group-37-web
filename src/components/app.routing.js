import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ForgotPassword } from './Auth/ForgotPassword/ForgotPassword.component';
import { LoginComponent } from './Auth/Login/Login.component';
import { RegisterComponent } from './Auth/Register/Register.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import { Header } from './Common/Header/Header.component';
import { PageNotFound } from './Common/PageNotFound/PageNotFound.component'
import { Sidebar } from './Common/Sidebar/Sidebar.component';
import { AddProduct } from './Products/AddProduct/AddProduct.component';
import { EditProduct } from './Products/EditProduct/EditProduct.component';
import { ProductDetailsLanding } from './Products/ProductDetailsLanding/ProductDetailsLanding.component';
import { SearchProduct } from './Products/SearchProduct/SearchProduct.component';
import { ViewProducts } from './Products/ViewProducts/ViewProducts.component';
import { Chat } from './Users/Chat/Chat.component';
import { Dashboard } from './Users/Dashboard/Dashboard.component';

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => {
            return localStorage.getItem('token')
                ? <div>
                    <Header isLoggedIn={true}></Header>
                    <div className="main">
                        <Sidebar></Sidebar>
                        <div className="content">
                            <Component {...routeProps}></Component>
                        </div>
                    </div>
                </div>
                : <Redirect to="/"></Redirect>
        }}></Route>
    )
}
const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => {
            return (
                <div>
                    <Header></Header>
                    <div className="main">
                        {/* sidebar miss huda ko css milaidinus */}
                        <div className="content">
                            <Component {...routeProps}></Component>
                        </div>
                    </div>
                </div>
            )
        }}></Route>
    )
}

export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact path="/" component={LoginComponent}></PublicRoute>
                <PublicRoute path="/register" component={RegisterComponent}></PublicRoute>
                <PublicRoute path="/forgot_password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset_password/:token" component={ResetPassword}></PublicRoute>
                <ProtectedRoute path="/chat" component={Chat}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewProducts}></ProtectedRoute>
                <ProtectedRoute path="/search_product" component={SearchProduct}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/product_details/:id" component={ProductDetailsLanding}></ProtectedRoute>
                <PublicRoute component={PageNotFound}></PublicRoute>
            </Switch>
        </BrowserRouter>
    )
}

// revise
// library ==> react-router-dom
// BrowserRouter==> wrapper for overall routing configuraton
// Route ==> config block for path and component
// exact ===> render component on exact path match
// Link/NavLink ==> click event navigation in react
// it will prevent page reload 

// props supplied by route
// history -==> functions to be invoked to navigate
// location => to access data passed via routing / optional route params
// match ==> access the dynamic value from placeholder we set during routing configuration

// link or navlink ma / le fark parcha
// / rakhda root bata path set huncha
// narakhda last value replace huncha

// todo
// supply route props(history match location) in component other then route specified eg header
// page not component