import React, { Component } from 'react';
import { ErrorHandler } from '../../../utils/error.handler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.component';

export class ForgotPassword extends Component {
    constructor() {
        super()

        this.state = {
            email: '',
            emailErr: '',
            isSubmitting: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        }, () => {
            let err = this.state.email
                ? this.state.email.includes('@') && this.state.email.includes('.com')
                    ? ''
                    : 'invalid email'
                : 'required field*';

            this.setState({
                emailErr: err
            })
        })
    }

    send = e => {
        e.preventDefault();
        const { email } = this.state;
        if (!email) return;
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/forgot-password', { email })
            .then(response => {
                notify.showInfo("Password reset link sent to your email please check your inbox");
                this.props.history.push('/');
            })
            .catch(err => {
                ErrorHandler(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div>
                <h2>Forgot Password</h2>
                <p>Please provide your email address to reset your password</p>
                <form noValidate onSubmit={this.send} className="form-group">
                    <label>Email</label>
                    <input type="text" name="email" placeholder="Email Address here..." className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.emailErr}</p>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={this.state.emailErr}
                    ></SubmitButton>
                </form>

            </div>
        );
    }
}