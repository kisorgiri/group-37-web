import { Component } from 'react';
import { Link } from 'react-router-dom';
import { SubmitButton } from './../../Common/SubmitButton/SubmitButton.component';
import { notify } from './../../../utils/notify'
import { ErrorHandler } from './../../../utils/error.handler'
import { redirectToDashboard } from '../../../services/redirection';
import { httpClient } from '../../../utils/httpClient';
// axios base url
// stateful component
// state ==> component's date to be preserved within component
const defaultForm = {
    username: '',
    password: ''
}
export class LoginComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            isSubmitting: false,
            remember_me: false
        }
    }
    componentDidMount() {
        let remember_me = localStorage.getItem('remember_me');
        if (remember_me === 'true') {
            this.props.history.push('home')
        }
    }

    handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            return this.setState({
                [name]: checked
            })
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            if (this.state.error[name]) {
                this.validateForm();
            }
        })
    }

    validateForm() {
        let usernameErr = this.state.data.username ? '' : 'required field*'
        let passwordErr = this.state.data.password ? '' : 'required field*'

        this.setState(prevState => ({
            error: {
                ...prevState.error,
                username: usernameErr,
                password: passwordErr
            }
        }))
        // setState is async
        return true && !usernameErr && !passwordErr;
    }

    submit(e) {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST(`/auth/login`, this.state.data)
            .then(response => {
                console.log("response is >>>", response)
                // series of task
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user))
                localStorage.setItem('remember_me', this.state.remember_me)
                redirectToDashboard(response.data.user.role, this.props.history)
            })
            .catch(err => {
                ErrorHandler(err);
                // todo handle error
                this.setState({
                    isSubmitting: false
                })
            })
    }

    // TODO learn lifecycle stages

    //render method is mandatory
    // render block will define UI
    // whenever there is change in either state or props render method is executed
    render() {
        // try to keep UI logic inside render and before return
        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to start using hamro application</p>
                <form className="form-group" onSubmit={this.submit.bind(this)}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" name="username" id="username" placeholder="Username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" name="password" id="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <input type="checkbox" name="remember_me" onChange={this.handleChange}></input>
                    <label> &nbsp;Remember Me</label>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        enabledLabel="Login"
                        disabledLabel="Login in..."
                    ></SubmitButton>
                </form>
                <br />
                <p>Don't have an account?</p>
                <p style={{ float: 'left' }}>Register <Link to="/register">here</Link></p>
                <p style={{ float: 'right' }}><Link to="/forgot_password">forgot password?</Link></p>

            </div>
        )
    }


}

