import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ErrorHandler } from '../../../utils/error.handler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.component';

const defaultForm = {
    password: '',
    confirmPassword: ''
}

export class ResetPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        }
    }
    componentDidMount() {
        this.resetToken = this.props.match.params['token'];
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name)
        })
    }

    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {

            case 'password':
                errMsg = this.state.data['confirmPassword']
                    ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data['password']
                    ? this.state.data[fieldName] === this.state.data['password']
                        ? ''
                        : 'password didnot match'
                    : this.state.data[fieldName]
                        ? this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                        : 'required field*'
                break;

            default:
                break;
        }

        this.setState(prevState => ({
            error: {
                ...prevState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object.values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })

        })
    }

    handleSubmit = e => {
        e.preventDefault();
        httpClient.POST(`/auth/reset-password/${this.resetToken}`, this.state.data)
            .then(response => {
                notify.showInfo("Password Reset Successfull please login.")
                this.props.history.push('/');
            })
            .catch(err => {
                ErrorHandler(err);
            })
    }

    render() {
        const { error } = this.state;
        return (
            <div>
                <h2>Reset Password</h2>
                <p>Please choose your new password</p>
                <form className="form-group" noValidate onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" name="confirmPassword" placeholder="Confirm Password" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{error.confirmPassword}</p>
                    <hr></hr>
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    ></SubmitButton>
                </form>
                <p>back to <Link to="/">login</Link></p>
            </div>
        );
    }
}
