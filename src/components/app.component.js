// root component
import React from 'react';
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux'
import { store } from '../store';


export const App = (args) => (
  <div>
    <Provider store={store}>
      <AppRouting></AppRouting>
      <ToastContainer></ToastContainer>
    </Provider>

  </div>
)


// jsx 1st lesson  use parenthesis to write multi line JSX
// { use of interpolation }


// state and props
// state and props are data of component
// state ==> data within a component
// props ==> incoming data for a component

// component
// Component is Basic building block of react
// component is responsible for defining a html node
// component will return single html node

// component can be written as
// functional component

// class based component


// component can be
// statefull component
// when within a component's data should be preserved
// prior then 16.8 version
// class based component are statefull component
// after 16.8 version hooks is introduced and functional component can have state

// stateless component
// data doesnot needs to be preserved
// TODO
// pure component its output will be same for same input
// ==> render ===> for same props purecomponent doesnot run render methods 

// pure function
// the output of function doesnot change on same input(argument passed)

// // pure function
// function add(num1, num2) {
//     return num1 + num2;
// }

// // impure function 
// function getMiliSeconds() {
//     return Date.now();
// }
// var intialNum = 2;
// function multiply(num1) {
//     return num1 * intialNum;
// }


// 16.8 
// class based component ==> statefull component 
// functional comonent  ==> statefull component

