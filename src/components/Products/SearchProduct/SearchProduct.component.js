import React, { Component } from 'react';
import { SubmitButton } from './../../Common/SubmitButton/SubmitButton.component'
import { httpClient } from './../../../utils/httpClient'
import { notify } from './../../../utils/notify'
import { ErrorHandler } from './../../../utils/error.handler';
import { ViewProducts } from './../ViewProducts/ViewProducts.component';

const defaultForm = {
    category: '',
    name: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    toDate: '',
    tags: '',
    multipleDateRange: '',
    brand: '',
    color: ''
}

export class SearchProduct extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            allProducts: [],
            categories: [],
            names: [],
            isValidFrom: false,
            isSubmitting: false,
            isCategoryLoading: false,
            searchResults: []
        }
    }
    componentDidMount() {
        this.setState({
            isCategoryLoading: true
        })
        httpClient.POST('/product/search', {})
            .then(response => {
                let cats = [];
                (response.data || []).forEach((item, index) => {
                    if (cats.indexOf(item.category) === -1) {
                        cats.push(item.category)
                    }
                })
                this.setState({
                    categories: cats,
                    allProducts: response.data
                })
            })
            .catch(err => {
                ErrorHandler(err);
            }).finally(() => {
                this.setState({
                    isCategoryLoading: false
                })
            })
    }

    handleChange = e => {
        let { name, value, type, checked } = e.target;
        if (name === 'category') {
            this.populateNames(value)
        }
        if (type === 'checkbox') {
            value = checked
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))
    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = data.fromDate
        }
        httpClient.POST('/product/search', data)
            .then(response => {
                console.log('response >>', response.data)
                if (!response.data.length) {
                    return notify.showInfo("No any products matched your search query!")
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                ErrorHandler(err)

            }).finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }

    populateNames = (selectedCategory) => {
        const { allProducts } = this.state;
        const names = allProducts.filter(item => item.category === selectedCategory);
        this.setState({
            names
        })
    }

    reset = () => {
        this.setState({
            searchResults: [],
            data: {
                ...defaultForm
            }
        })
    }

    render() {

        let content = this.state.searchResults.length > 0
            ? <ViewProducts productData={this.state.searchResults} resetSearch={this.reset}></ViewProducts>
            : <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Category</label>
                    <select name="category" className="form-control" onChange={this.handleChange}>
                        <option>(Select Category)</option>
                        {
                            this.state.categories.map((cat, index) => (
                                <option key={index} value={cat}>{cat}</option>
                            ))
                        }
                    </select>
                    {
                        this.state.data.category && this.state.names.length && (
                            <>
                                <label>Name</label>
                                <select name="name" className="form-control" onChange={this.handleChange}>
                                    <option>(Select Name)</option>
                                    {
                                        this.state.names.map((item, index) => (
                                            <option key={index} value={item.name}>{item.name}</option>
                                        ))
                                    }
                                </select>
                            </>
                        )
                    }

                    <label>Min Price</label>
                    <input type="number" name="minPrice" className="form-control" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input type="number" name="maxPrice" className="form-control" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" name="color" className="form-control" onChange={this.handleChange}></input>
                    <label>Brand</label>
                    <input type="text" name="brand" className="form-control" onChange={this.handleChange}></input>
                    <label>Select Date</label>
                    <input type="date" name="fromDate" className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>&nbsp;Multiple Date Range</label>
                    <br />
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input type="date" name="toDate" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )
                    }
                    <label>Tags</label>
                    <input type="text" name="tags" className="form-control" placeholder="Tags" onChange={this.handleChange}></input>

                    <hr></hr>
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        enabledLabel="Search"
                        disabledLabel="Searching..."
                    ></SubmitButton>
                </form>

            </>
        return content;
    }
}

