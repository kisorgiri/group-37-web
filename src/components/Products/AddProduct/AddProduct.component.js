import React, { Component } from 'react';
import { ProductForm } from '../ProductForm/ProductForm.component';
import { ErrorHandler } from './../../../utils/error.handler'
import { httpClient } from './../../../utils/httpClient'
import { notify } from './../../../utils/notify'

export class AddProduct extends Component {
    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }

    add = (data, files) => {
        console.log("here in add prodcut component >>", data)
        // httpcall
        this.setState({
            isSubmitting: true
        })
        httpClient.UPLOAD('POST','/product', data, files)
            .then(response => {
                notify.showSuccess('Product Added Successfully')
                this.props.history.push('view_products')
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                })
                ErrorHandler(err)
            })
    }

    render() {
        return (
            <ProductForm
                isSubmitting={this.state.isSubmitting}
                submitCallback={this.add}
            ></ProductForm>
        );
    }
}

