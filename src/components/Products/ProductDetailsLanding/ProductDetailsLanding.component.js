import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchProduct_ac, addReview_ac } from '../../../actions/product.ac';
import { Loader } from './../../Common/Loader/Loader.component'
import { AddReview } from './AddReview/AddReview.component';
import { Details } from './Details/Details.component';

class ProductDetailsLandingComponent extends Component {
    componentDidMount() {
        this.productId = this.props.match.params['id'];
        this.props.fetchProduct(this.productId)
    }

    submitReview = reviewData => {
        console.log('review data >>', reviewData)
        this.props.addReview(reviewData, this.productId)
        // api call through action file and update in details component
    }

    render() {
        let content = this.props.isProductLoading
            ? <Loader></Loader>
            : <>
                <Details product={this.props.product}></Details>
                <hr></hr>
                <AddReview submitting={this.props.isReviewSubmitting} addReview={this.submitReview}></AddReview>
            </>
        return content;
    }
}

const mapStateToProps = rootStore => ({
    isProductLoading: rootStore.product.isProductLoading,
    product: rootStore.product.product,
    isReviewSubmitting: rootStore.product.isReviewSubmitting
})

const mapDispatchToProps = dispatch => ({
    fetchProduct: (id) => dispatch(fetchProduct_ac(id)),
    addReview: (reviewData, productId) => dispatch(addReview_ac(reviewData, productId))
})

export const ProductDetailsLanding = connect(mapStateToProps, mapDispatchToProps)(ProductDetailsLandingComponent)

