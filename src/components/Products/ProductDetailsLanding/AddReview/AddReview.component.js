import React, { Component } from 'react';
import { SubmitButton } from '../../../Common/SubmitButton/SubmitButton.component'

const defaultForm = {
    reviewPoint: '',
    reviewMessage: ''
}
export class AddReview extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            }
        }
    }


    handleChange = e => {
        const { name, value } = e.target;

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.addReview(this.state.data);
        this.setState({
            data: {
                ...defaultForm
            }
        })
    }
    render() {
        return (
            <div>
                <h2>Add Review</h2>
                <form onSubmit={this.handleSubmit} className="form-group" noValidate>
                    <label>Point</label>
                    <input className="form-control" value={this.state.data.reviewPoint} type="number" min="1" max="5" name="reviewPoint" onChange={this.handleChange}></input>
                    <label>Messages</label>
                    <input className="form-control" value={this.state.data.reviewMessage} type="text" name="reviewMessage" onChange={this.handleChange} placeholder="Rating Messgae here..."></input>
                    <br></br>
                    <SubmitButton
                        isSubmitting={this.props.submitting}
                    ></SubmitButton>
                </form>

            </div>
        );
    }
}
