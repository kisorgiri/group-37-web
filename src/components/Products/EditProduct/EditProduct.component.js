import React, { Component } from 'react';
import { ErrorHandler } from '../../../utils/error.handler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { Loader } from '../../Common/Loader/Loader.component';
import { ProductForm } from '../ProductForm/ProductForm.component';

export class EditProduct extends Component {
    constructor() {
        super()

        this.state = {
            isLoading: false,
            product: {},
            isSubmitting: false
        }
    }
    async componentDidMount() {
        this.productId = this.props.match.params['id']
        this.setState({
            isLoading: true
        })
        let response;
        try {
            response = await httpClient.GET(`/product/${this.productId}`, true)
            this.setState({
                product: response.data
            })
            // 
        } catch (err) {
            ErrorHandler(err)
        } finally {
            this.setState({
                isLoading: false
            })
        }

    }

    edit = (data, filesToUpload = [], filesToRemove = []) => {
        this.setState({
            isSubmitting: true
        })
        const requestData = {
            ...data,
            vendor: data.vendor._id,
            filesToRemove
        }
        httpClient.UPLOAD('PUT', `/product/${this.productId}`, requestData, filesToUpload)
            .then(response => {
                notify.showInfo("Product Updated Successfully");
                this.props.history.push('/view_products')
            })
            .catch(err => {
                ErrorHandler(err)
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm
                isEdit={true}
                submitCallback={this.edit}
                isSubmitting={this.state.isSubmitting}
                productData={this.state.product}
            ></ProductForm>
        return content;
    }
}
