import React, { Component } from 'react';
import { ErrorHandler } from '../../../utils/error.handler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { FaPencilAlt, FaTrashAlt } from 'react-icons/fa'
import { Link } from 'react-router-dom';
import { Loader } from '../../Common/Loader/Loader.component';
import { formatDate } from '../../../utils/dateUtil';
import { fetchProducts_ac, removeProduct_ac } from './../../../actions/product.ac'
import { connect } from 'react-redux';

const IMG_URL = process.env.REACT_APP_IMG_URL;

class ViewProductsComponent extends Component {

    componentDidMount() {
        console.log('check props in view product component', this.props)
        this.props.fetch();
        // if (this.props.productData) {
        //     return this.setState({
        //         products: this.props.productData
        //     })
        // }
        // this.setState({
        //     isLoading: true
        // })

    }

    editProduct = id => {
        this.props.history.push(`/edit_product/${id}`)
    }

    removeProduct = (id) => {
        let confirmation = window.confirm("Are you sure to remove?");
        if (confirmation) {

            this.props.removeProduct_ac(id)
        }
        // http call
    }

    render() {
        let content = this.props.isLoading
            ? <Loader circular={true} message="show"></Loader>
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Tags</th>
                        <th>Images</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {(this.props.products || []).map((item, index) => (
                        <tr key={item._id}>
                            <td>{index + 1}</td>
                            <td><Link to={`/product_details/${item._id}`}>{item.name}</Link> </td>
                            <td>{item.category}</td>
                            <td>{item.price}</td>
                            <td>{formatDate(item.createdAt, 'ddd YYYY/MM/DD hh:mm a')}</td>
                            <td>{item.tags.join(',')}</td>
                            <td>
                                <img src={`${IMG_URL}${item.images[0]}`} alt="product.jpg" width="200px"></img>
                            </td>
                            <td>
                                <span onClick={() => this.editProduct(item._id)} title="Edit Product" style={{ color: 'blue' }}> <FaPencilAlt></FaPencilAlt></span>
                                <span onClick={() => this.removeProduct(item._id, index)} title="Remove Product" style={{ marginLeft: '5px', color: 'red' }}><FaTrashAlt></FaTrashAlt></span>

                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        return (
            <>
                <h2>View Products</h2>
                {this.props.productData && (
                    <button onClick={this.props.resetSearch} className="btn btn-success" >Search Again</button>
                )}
                {content}
            </>
        );
    }
}

// map state to props
// incoiming data from store to component [as in props]
const mapStateToProps = rootStore => ({
    isLoading: rootStore.product.isLoading,
    products: rootStore.product.products
})

// map dispatch to Props
// outgoing event from component
const mapDispatchToProps = dispatch => ({
    fetch: (params) => dispatch(fetchProducts_ac(params)),
    removeProduct_ac: (id) => dispatch(removeProduct_ac(id))
})

export const ViewProducts = connect(mapStateToProps, mapDispatchToProps)(ViewProductsComponent)