import React, { Component } from 'react';
import { FaTrashAlt } from 'react-icons/fa';
import { formatDate } from '../../../utils/dateUtil';
import { SubmitButton } from './../../Common/SubmitButton/SubmitButton.component';

const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultForm = {
    name: '',
    category: '',
    price: '',
    brand: '',
    color: '',
    costPrice: '',
    description: '',
    quantity: '',
    modelNo: '',
    sku: '',
    manuDate: '',
    expiryDate: '',
    purchasedDate: '',
    salesDate: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
    isReturnEligible: '',
    warrentyStatus: '',
    warrentyPeriod: '',
    origin: '',
    tags: '',
    offers: '',
}

export class ProductForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            filesToUpload: [],
            filesToPreview: [],
            filesToRemove: []
        }
    }

    componentDidMount() {
        const { productData } = this.props;
        if (productData) {
            // edit case
            let previousImages = [];
            if (productData.images) {
                previousImages = productData.images.map(item => `${IMG_URL}${item}`)
            }
            this.setState({
                data: {
                    ...defaultForm,
                    ...productData,
                    discountedItem: productData.discount && productData.discount.discountedItem
                        ? productData.discount.discountedItem
                        : false,
                    discountType: productData.discount && productData.discount.discountType
                        ? productData.discount.discountType
                        : '',
                    discountValue: productData.discount && productData.discount.discountValue
                        ? productData.discount.discountValue
                        : '',
                    manuDate: productData.manuDate ? formatDate(productData.manuDate, 'YYYY-MM-DD') : '',
                    expiryDate: productData.expiryDate ? formatDate(productData.expiryDate, 'YYYY-MM-DD') : '',
                    purchasedDate: productData.purchasedDate ? formatDate(productData.purchasedDate, 'YYYY-MM-DD') : '',
                    salesDate: productData.salesDate ? formatDate(productData.salesDate, 'YYYY-MM-DD') : ''
                },
                filesToPreview: previousImages
            })
        }
    }

    handleChange = e => {
        let { name, value, type, checked, files } = e.target;

        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0])

            this.setState({
                filesToUpload
            })
        }
        if (type === 'checkbox') {
            value = checked
        }
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name)
        })
    }

    validateForm = (fieldName) => {
        // TODO
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload, this.state.filesToRemove)
    }

    removeImage = (type, index, file) => {
        const { filesToPreview, filesToUpload, filesToRemove } = this.state;

        if (type === 'new') {
            // remove file form filetoUplaod
            filesToUpload.splice(index, 1)
        }
        if (type === 'old') {
            // maintain different allocation to store inofmration about file to remove from server
            filesToPreview.splice(index, 1)
            filesToRemove.push(file)
        }
        // index for removing item from fileToPreview
        this.setState({
            filesToUpload,
            filesToPreview,
            filesToRemove
        })
    }

    render() {
        const { isEdit, isSubmitting } = this.props;
        const { data } = this.state;
        const title = `${isEdit ? 'Update' : 'Add'} Product`;
        let discountContent = this.state.data.discountedItem
            ? <>
                <label>Discount Type</label>
                <select name="discountType" value={data.discountType} className="form-control" onChange={this.handleChange}>
                    <option value="">(Select Discount Type)</option>
                    <option value="percentage">Percentage</option>
                    <option value="quantity">Quantity</option>
                    <option value="value">Value</option>
                </select>
                <label>Discount Value</label>
                <input type="text" value={data.discountValue} name="discountValue" placeholder="Discount Value" className="form-control" onChange={this.handleChange}></input>
            </>
            : ''
        return (
            <>
                <h2>{title}</h2>
                <p>{`Please ${isEdit ? 'Update' : 'Add'} necessary details`}</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" name="name" value={data.name} placeholder="Name" className="form-control" onChange={this.handleChange}></input>
                    <label>Description</label>
                    <textarea rows={8} name="description" value={data.description} placeholder="Description" className="form-control" onChange={this.handleChange}></textarea>
                    <label>Category</label>
                    <input type="text" name="category" value={data.category} placeholder="Category" className="form-control" onChange={this.handleChange}></input>
                    <label>Brand</label>
                    <input type="text" name="brand" value={data.brand} placeholder="Brand" className="form-control" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" name="color" value={data.color} placeholder="Color" className="form-control" onChange={this.handleChange}></input>
                    <label>Price</label>
                    <input type="number" name="price" value={data.price} placeholder="Price" className="form-control" onChange={this.handleChange}></input>
                    <label>Cost Price</label>
                    <input type="number" name="costPrice" value={data.costPrice} placeholder="Cost Price" className="form-control" onChange={this.handleChange}></input>
                    <label>Quantity</label>
                    <input type="number" name="quantity" value={data.quantity} placeholder="Quantity" className="form-control" onChange={this.handleChange}></input>
                    <label>Model No.</label>
                    <input type="text" name="modelNo" value={data.modelNo} placeholder="Model No." className="form-control" onChange={this.handleChange}></input>
                    <label>SKU Number</label>
                    <input type="text" name="sku" value={data.sku} placeholder="SKU Number" className="form-control" onChange={this.handleChange}></input>
                    <label>Manu Date</label>
                    <input type="date" name="manuDate" value={data.manuDate} className="form-control" onChange={this.handleChange}></input>
                    <label>Expiry Date</label>
                    <input type="date" name="expiryDate" value={data.expiryDate} placeholder="Expiry Date" className="form-control" onChange={this.handleChange}></input>
                    <label>Purchased Date</label>
                    <input type="date" name="purchasedDate" value={data.purchasedDate} className="form-control" onChange={this.handleChange}></input>
                    <label>Sales Date</label>
                    <input type="date" name="salesDate" value={data.salesDate} className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" checked={data.discountedItem} name="discountedItem" onChange={this.handleChange}></input>
                    <label>&nbsp;Discounted Item</label>
                    <br />
                    {discountContent}
                    <label>Offers</label>
                    <input type="text" name="offers" value={data.offers} placeholder="Offers" className="form-control" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input type="text" name="tags" value={data.tags} placeholder="Tags" className="form-control" onChange={this.handleChange}></input>
                    <label>Origin</label>
                    <input type="text" name="origin" value={data.origin} placeholder="Origin" className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" checked={data.warrentyStatus} name="warrentyStatus" onChange={this.handleChange}></input>
                    <label> &nbsp;Warrenty Status</label>
                    <br />
                    {
                        this.state.data.warrentyStatus && (
                            <>
                                <label>Warrenty Period</label>
                                <input type="text" value={data.warrentyPeriod} name="warrentyPeriod" placeholder="Warrenty Period" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )

                    }

                    <input type="checkbox" checked={data.isReturnEligible} name="isReturnEligible" onChange={this.handleChange}></input>
                    <label> &nbsp;Return Eligible</label>
                    <br></br>
                    <label>Choose Images</label>
                    <input type="file" className="form-control" onChange={this.handleChange} accept="image/*"></input>
                    {
                        this.state.filesToPreview.map((file, index) => (
                            <div style={{ marginTop: '10px' }} key={index} >
                                <img src={file} alt="preview.png" width="200px"></img>
                                <span onClick={() => this.removeImage('old', index, file)} title="Remove Image" style={{ marginLeft: '5px', color: 'red' }}>
                                    <FaTrashAlt></FaTrashAlt>
                                </span>
                            </div>
                        ))
                    }
                    {
                        this.state.filesToUpload.map((file, index) => (
                            <div style={{ marginTop: '10px' }} key={index} >
                                <img src={URL.createObjectURL(file)} alt="preview.png" width="200px"></img>
                                <span onClick={() => this.removeImage('new', index)} title="Remove Image" style={{ marginLeft: '5px', color: 'red' }}>
                                    <FaTrashAlt></FaTrashAlt>
                                </span>
                            </div>
                        ))
                    }
                    <hr />
                    <SubmitButton
                        isSubmitting={isSubmitting}
                    // isDisabled={!this.state.isValidForm}
                    ></SubmitButton>
                </form>

            </>
        );
    }
}

// current scenario
// two different allocation to hold images
// fileToUpload ==> to upload new images
// fileToPreview ==> to preview selected and existing images

// todo
// on edit
// once existing images is removed from UI it should be remved from database as well
// once newly added images is remove in edit screen it should not be uploaded in server

