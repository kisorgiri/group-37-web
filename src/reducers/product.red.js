import { ProductActionTypes } from "../actions/product.ac"


export const ProductReducer = (state = {}, action) => {
    console.log('state is >>>', state)
    console.log('at reducer >>>>', action)
    switch (action.type) {
        case ProductActionTypes.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }

        case ProductActionTypes.PRODUCTS_RECEIVED:
            return {
                ...state,
                products: action.payload
            }

        case ProductActionTypes.PROUDCT_REMOVED:
            const { products } = state;
            products.forEach((item, index) => {
                if (item._id === action.payload) {
                    products.splice(index, 1)
                }
            })
            return {
                ...state,
                products: [...products]
            }

        case ProductActionTypes.PRODUCT_RECEIVED:
            return {
                ...state,
                product: action.payload
            }
        case ProductActionTypes.SET_IS_PRODUCT_LOADING:
            return {
                ...state,
                isProductLoading: action.payload
            }
        case ProductActionTypes.SET_IS_REVIEW_SUBMITTING:
            return {
                ...state,
                isReviewSubmitting: action.payload
            }

        default:
            return {
                ...state
            }
    }
}

