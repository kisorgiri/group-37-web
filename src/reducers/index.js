import { combineReducers } from 'redux';
import { ProductReducer } from './product.red';

export const rootReducer = combineReducers({
    product: ProductReducer
})