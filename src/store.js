import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from './reducers';

const middlewares = [thunk]
const initialState = {
    product: {
        isLoading: false,
        isProductLoading: false,
        isReviewSubmitting: false,
        product: {},
        products: []
    }
}

export const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares))

// how to maintain store
// 
// {
//     product:'',
//     isLoading:'',
//     users:[],
//     userIsloading:''
// }

// {
//     product:{
//         isloading:false,
//         records:[]
//     },
//     notifications:{

//     },
//     users:{
//         isloading:false,
//         users:[]
//     }
// }