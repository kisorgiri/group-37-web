// component life cycle
// 3 stages of component
// ################# Init #########################
// constructor
// render
// componentDidMount ===> component fully load huda slef invoked function


// ######################Update #########################
// either props change or state change it is termed as component updated
// render
// componentDidUpdate ==> self invoked function once component data is changed
// componentDidUpdate can take arguments aswell,
// first argument is always previous props
// 2nd arugment is always previous state


// Destroy
// componentWillUnmount


// redux 

// FLUX architecture
// views, Actions, Dispatcher , Store
// views ==> actions===> dispatcher==> store

// unidirectional data flow
// there can be multiple store
// disapatcher ==> action bata aayeko paylod dispatch garcha to store
// store holds the data and logic to update centralized

// REDUX ===> state management tool 
// REDUX is built on top of FLUX

// REDUX ===> views, actions, reducer, store

// unidirectional data flow
// views ==> actions===> reducer==> store
// redux holds single store
// reducer holds logic to update store

// dependencies
// redux ==> independant library
// react-redux===> glue to connect react and redux
// middleware ==> thunk ,saga==> enhance